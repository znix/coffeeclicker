/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clickbot;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;

/**
 *
 * @author campbell
 */
public class UI {

    private final JFrame frame;
    private final JButton toggle;
    private final JSlider rate;

    public UI() {
        JPanel contentPane = new JPanel(new BorderLayout(5, 5));

        Border border = BorderFactory.createBevelBorder(BevelBorder.RAISED);
        border = BorderFactory.createCompoundBorder(border, BorderFactory.createEmptyBorder(10, 10, 10, 10));
        border = BorderFactory.createTitledBorder(border, "ClickBot");
        contentPane.setBorder(border);

        toggle = new JButton(stop);
        contentPane.add(toggle, BorderLayout.CENTER);

        contentPane.add(new JButton(new AbstractAction("Exit") {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        }), BorderLayout.EAST);

        rate = new JSlider(0, 500, 40);
        rate.setMajorTickSpacing(100);
        rate.setMinorTickSpacing(20);
        rate.setSnapToTicks(true);
        rate.setPaintTicks(true);
        rate.setPaintLabels(true);
        contentPane.add(rate, BorderLayout.NORTH);

        frame = new JFrame("ClickBot");

        frame.setContentPane(contentPane);
        frame.setAlwaysOnTop(true);
        frame.setResizable(false);
        frame.setSize(300, 125);
        frame.setVisible(true);
        frame.setLocation(15, 15);

        startStop(false);
    }

    public void setTimeTilRun(int sec) {
        if (sec == 0) {
            stop.putValue(Action.NAME, "Stop");
        } else {
            stop.putValue(Action.NAME, "Cancel (" + sec + ")");
        }
    }

    private void startStop(boolean run) {
        setTimeTilRun(0);

        if (run) {
            Clickbot.clickThread = new Clickbot.ClickThread(rate.getValue(), this);
            Clickbot.clickThread.start();

            toggle.setAction(stop);
        } else {
            Clickbot.clickThread = null;

            toggle.setAction(start);
        }

        frame.dispose();
        frame.setUndecorated(run);
        frame.setVisible(true);
        
        rate.setEnabled(!run);
    }

    private final AbstractAction start = new AbstractAction("Start") {
        @Override
        public void actionPerformed(ActionEvent e) {
            startStop(true);
        }
    };

    private final AbstractAction stop = new AbstractAction(null) {
        @Override
        public void actionPerformed(ActionEvent e) {
            startStop(false);
        }
    };
}
