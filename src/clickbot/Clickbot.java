/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clickbot;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author campbell
 */
public class Clickbot {

    public static Thread clickThread;

    /**
     * @param args the command line arguments
     * @throws java.lang.InterruptedException
     * @throws java.awt.AWTException
     */
    public static void main(String[] args) throws InterruptedException, AWTException {
        int speed = 50;

        UI ui = new UI();

        Object o1 = new Object();

        synchronized (o1) {
            o1.wait();
        }
    }

    public static class ClickThread extends Thread {

        private final int speed;
        private final UI ui;

        public ClickThread(int speed, UI ui) {
            this.speed = speed;
            this.ui = ui;
        }

        @Override
        public void run() {
            try {
                for (int i = 0; i < 5; i++) {
                    ui.setTimeTilRun(5 - i);
                    if (clickThread != this) {
                        break;
                    }
                    Thread.sleep(1000);
                }
                ui.setTimeTilRun(0);
                int delay = 1000 / speed / 2;
                Robot robo = new Robot();
                for (;;) {
                    if (clickThread != this) {
                        break;
                    }
                    robo.mousePress(InputEvent.BUTTON1_DOWN_MASK);
                    Thread.sleep(delay);
                    robo.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
                    Thread.sleep(delay);
                }
            } catch (AWTException | InterruptedException ex) {
                Logger.getLogger(Clickbot.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                ui.setTimeTilRun(0);
            }
        }
    }
}
